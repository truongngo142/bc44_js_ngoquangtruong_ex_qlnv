// get information from user
function layThongTin() {
  var tknv = document.getElementById("tknv").value;
  var ten = document.getElementById("name").value;
  var mail = document.getElementById("email").value;
  var matKhau = document.getElementById("password").value;
  var ngay = document.getElementById("datepicker").value;
  var luongCB = document.getElementById("luongCB").value * 1;
  var chucVu = document.getElementById("chucvu").value;
  var gioLam = document.getElementById("gioLam").value;

  var nv = new Nhanvien(
    tknv,
    ten,
    mail,
    matKhau,
    ngay,
    luongCB,
    chucVu,
    gioLam
  );
  return nv;
}

// render dssv
function renderDSNV(dsnv) {
  var contentHTML = "";
  for (i = 0; i < dsnv.length; i++) {
    var nv = dsnv[i];
    // nv là item của array DSNV
    var contentTr = `<tr>
    <td>${nv.tknv}</td>
    <td>${nv.ten}</td>
    <td>${nv.mail}</td>
    <td>${nv.ngay}</td>
    <td>${nv.chucVu}</td>
    <td>${nv.tongLuong()}</td>
    <td>${nv.xepLoai()}</td>
    <td>
        <button class="btn btn-primary" onclick="suaNV(${
          nv.tknv
        })"data-toggle="modal" data-target="#myModal">Sửa</button>
        <button class="btn btn-danger" onclick="xoaNV(${nv.tknv})" >Xoá</button>
    </td>
    </tr>`;
    contentHTML += contentTr;
  }
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}

// show information
function showThongTin(nv) {
  document.getElementById("tknv").value = nv.tknv;
  document.getElementById("name").value = nv.ten;
  document.getElementById("email").value = nv.mail;
  document.getElementById("password").value = nv.matKhau;
  document.getElementById("datepicker").value = nv.ngay;
  document.getElementById("luongCB").value = nv.luongCB;
  document.getElementById("chucvu").value = nv.chucVu;
  document.getElementById("gioLam").value = nv.gioLam;
}
