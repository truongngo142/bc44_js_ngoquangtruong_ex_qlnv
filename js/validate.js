//show thong tin
var showMessage = function (id, message) {
  document.getElementById(id).innerHTML = message;
};
//kiem tra trung
var kiemTratrung = function (tknv, dsnv) {
  var index = dsnv.findIndex(function (item) {
    return tknv == item.tknv;
  });
  if (index == -1) {
    showMessage("tbTKNV", "");
    return true;
  } else {
    showMessage("tbTKNV", "Tài khoản đã tồn tại");
    return false;
  }
};

// Kiểm tra rỗng
var kiemTraRong = function (idErr, value) {
  if (value.length == 0 || value == 0) {
    showMessage(idErr, "Hãy điền vào chỗ trống");
    return false;
  } else {
    showMessage(idErr, "");
    return true;
  }
};

// kiểm tra tên
var kiemTraTen = function (name) {
  const ten = /^(?=.*?[a-z])/;
  if (ten.test(name)) {
    showMessage("tbTen", "");
    return true;
  } else {
    showMessage("tbTen", "Tên nhân viên phải là chữ !");
    return false;
  }
};

// kiểm tra mail
var kiemTraEmail = function (email) {
  const re =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  if (re.test(email)) {
    showMessage("tbEmail", "");
    return true;
  } else {
    showMessage("tbEmail", "Xin vui lòng nhập đúng email !");
    return false;
  }
};

// kiểm tra mật khẩu
var kiemTraMK = function (password) {
  const mk = /^(?=.*\d)(?=.*[A-Z])(?=.*\W)[\dA-Za-z\W]{6,10}$/;
  if (mk.test(password)) {
    showMessage("tbMatKhau", "");
    return true;
  } else {
    showMessage(
      "tbMatKhau",
      "Mật khẩu bắt buộc 6-10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)"
    );
    return false;
  }
};

// Kiểm tra lương
var kiemTraLuong = function (salary) {
  if (salary >= 1000000 && salary <= 20000000) {
    showMessage("tbLuongCB", "");
    return true;
  } else {
    showMessage(
      "tbLuongCB",
      "Lương cơ bản nhân viên phải từ 1.000.000 - 20.000.000!"
    );
    return false;
  }
};
// Kiểm tra chức vụ
var kiemTraChucVu = function (chucVu) {
  if (chucVu != "Chọn chức vụ") {
    showMessage("tbChucVu", "");
    return true;
  } else {
    showMessage("tbChucVu", "hãy chọn chức vụ của bạn ");
    return false;
  }
};
// Kiểm tra giờ làm
var kiemTraGioLam = function (time) {
  if (time >= 80 && time <= 200) {
    showMessage("tbGiolam", "");
    return true;
  } else {
    showMessage(
      "tbGiolam",
      "Số giờ làm trong tháng nhân viên phải từ 80 - 200!"
    );
    return false;
  }
};
