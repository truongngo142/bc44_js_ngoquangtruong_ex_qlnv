var dsnv = [];
// get JSON khi load trang
var dataJson = localStorage.getItem("DSNV");

if (dataJson != null) {
  // convert JSON=>array
  var dataArr = JSON.parse(dataJson);
  for (var i = 0; i < dataArr.length; i++) {
    var item = dataArr[i];
    var nv = new Nhanvien(
      item.tknv,
      item.ten,
      item.mail,
      item.matKhau,
      item.ngay,
      item.luongCB,
      item.chucVu,
      item.gioLam
    );
    dsnv.push(nv);
  }
}
renderDSNV(dsnv);

// Thêm nhân viên
function themNhanVien() {
  var nv = layThongTin();
  var isValid = kiemTratrung(nv.tknv, dsnv) && kiemTraRong("tbTKNV", nv.tknv);
  // tên
  isValid = isValid & kiemTraRong("tbTen", nv.ten) && kiemTraTen(nv.ten);
  // mail
  isValid = isValid & kiemTraRong("tbEmail", nv.mail) && kiemTraEmail(nv.mail);
  // mật khẩu
  isValid =
    isValid & kiemTraRong("tbMatKhau", nv.matKhau) && kiemTraMK(nv.matKhau);
  // lương
  isValid =
    isValid & kiemTraRong("tbLuongCB", nv.luongCB) && kiemTraLuong(nv.luongCB);
  // chức vụ
  isValid = isValid & kiemTraChucVu(nv.chucVu);
  // giờ làm
  isValid =
    isValid & kiemTraRong("tbGiolam", nv.gioLam) && kiemTraGioLam(nv.gioLam);
  if (isValid == true) {
    dsnv.push(nv);
    //convert array thành JSON
    var dataJson = JSON.stringify(dsnv);
    // save JSON
    localStorage.setItem("DSNV", dataJson);
    renderDSNV(dsnv);
  }
}
// xoá nhân viên
function xoaNV(idnv) {
  var viTri = -1;
  for (var i = 0; i < dsnv.length; i++) {
    var nv = dsnv[i];
    if (nv.tknv == idnv) {
      viTri = i;
      break;
    }
  }
  dsnv.splice(viTri, 1);
  var dataJson = JSON.stringify(dsnv);
  // save JSON
  localStorage.setItem("DSNV", dataJson);
  renderDSNV(dsnv);
}

// Sửa nhân viên
function suaNV(idnv) {
  var viTri = dsnv.findIndex(function (item) {
    return item.tknv == idnv;
  });
  if (viTri != -1) {
    showThongTin(dsnv[viTri]);
  }
  document.getElementById("tknv").disabled = true;
}

// cập nhật sinh viên
document.getElementById("btnCapNhat").onclick = function () {
  var nv = layThongTin();
  var viTri = dsnv.findIndex(function (item) {
    return item.tknv == nv.tknv;
  });
  if (viTri != -1) {
    dsnv[viTri] = nv;
  }
  var dataJson = JSON.stringify(dsnv);
  // save JSON
  localStorage.setItem("DSNV", dataJson);
  renderDSNV(dsnv);
};

// reset form
document.getElementById("formQLNV");
// search

document.getElementById("btnTimNV").onclick = function searchNV() {
  var request = document.getElementById("searchName").value.trim();
  document.getElementById("searchName").value = "";
  var result = dsnv.filter(function (e) {
    return e.xepLoai() === request;
  });
  renderDSNV(result);
};
