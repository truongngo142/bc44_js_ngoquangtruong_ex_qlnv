function Nhanvien(
  _tknv,
  _ten,
  _mail,
  _matKhau,
  _ngay,
  _luongCB,
  _chucVu,
  _gioLam
) {
  this.tknv = _tknv;
  this.ten = _ten;
  this.mail = _mail;
  this.matKhau = _matKhau;
  this.ngay = _ngay;
  this.luongCB = _luongCB;
  this.chucVu = _chucVu;
  this.gioLam = _gioLam;
  this.tongLuong = function () {
    if (this.chucVu === "Sếp") {
      return this.luongCB * 3;
    } else if (this.chucVu === "Trưởng phòng") {
      return this.luongCB * 2;
    } else {
      return this.luongCB;
    }
  };
  this.xepLoai = function () {
    if (this.gioLam >= 192) {
      return "Xuất sắc";
    } else if (this.gioLam >= 176) {
      return "Giỏi";
    } else if (this.gioLam >= 160) {
      return "Khá";
    } else {
      return "Trung bình";
    }
  };
}
